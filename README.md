# Déscription

Librairie ORM SQL ainsi qu'un builder de requête  

## Installation

```php
    require_once CHEMIN_DE_LA_LIB."Functions/Autoloader.php";
    Database_Autoloader::register();
```

## Usage

```php
\Functions\Tests::isMail( $_['mail'] );
\Functions\Files::folder_sizeReadble(__DIR__);
```


## License
[MIT](https://choosealicense.com/licenses/mit/)