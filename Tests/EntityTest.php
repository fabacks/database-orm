<?php

namespace Database;
use \PHPUnit\Framework\TestCase;
use \User;

class EntityTest extends TestCase
{

    public function test_InstallAndcheckTable(): void 
    {
    	\User::create();
    
        if ( !\User::checkTable() )
        	throw new InvalidArgumentException( "La table user n'existe pas." );
        
    }

    public function test_CreateUser(): void {
        $user = New User();
        $user->login  = "test";
        $user->old    = "26";
        $user->save();
    
        if ($user == 0)
    	    throw new InvalidArgumentException( "Impossible de créer l'utilisateur" );
        
    }
    
    public function test_UpdateUserById(): void {
    	$user = \User::getById(1);
    
        if ( empty($user) || $user == false) {
        	throw new InvalidArgumentException( "Impossible de charger l'utilisateur" );
        }
    
        $user->login  = "Fabs";
        $user->old    = "26";
        $user->save();
    
        if ($user == 0 || $user->login != "Fabs")
          	throw new InvalidArgumentException( "Impossible de modifier l'utilisateur" );
        
    }
    
    public function test_DeleteUserById(): void {
        \User::deleteById(1);
        $user = \User::getById(1);
    
        if (!empty($user) || $user->id <> 0 )
        	throw new InvalidArgumentException( "Impossible de supprimer l'utilisateur" );
        
    }
    
    public function test_DropUserTable(): void {
        \User::drop();
    
        if ( \User::checkTable() )
    		throw new InvalidArgumentException( "La table user n'a pas était supprimée." );
        
    }
    
}