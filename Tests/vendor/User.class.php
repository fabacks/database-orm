<?php


class User extends \Entity 
{
  public $id, $login, $old;
  public $TABLE_NAME = 'user';	
  protected $fields = array(
      'id' 		=> 'key',
      'login' => 'string',
      'old' 	=> 'int'
  );

}