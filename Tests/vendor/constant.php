<?php

// Constant connexion database

define('ENTITY_TYPE', 'mysql');                        // Définie le type de SGBD : mysql, sqlite
define('ENTITY_PREFIX', '');                            // Permet de préfixer automatiquement toutes les tables
define('ENTITY_HOST','localhost');                               // Ip d'accès "127.0.0.1" ou chemin si sqlite
define('ENTITY_PORT','3306');                           // Port défaut Mysql 3306 | inutile si sqlite
define('ENTITY_LOGIN','root');                              // Vide si sqlite
define('ENTITY_MDP','');                                // Vide si sqlite 
define('ENTITY_BDD','entity_test');                            // Le nom de la base de donnée sqlite/mysql