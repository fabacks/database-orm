<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'Tests'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'constant.php';
require_once __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

require_once __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'EntityConnector.class.php';
require_once __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Entity.class.php';

require_once __DIR__.DIRECTORY_SEPARATOR.'Tests'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'User.class.php';

mb_internal_encoding('UTF-8'); 
date_default_timezone_set('UTC');



/**
 * Suppression de toutes les tables restantes potentielles
 */
$query = "
SET FOREIGN_KEY_CHECKS = 0;
SET GROUP_CONCAT_MAX_LEN=32768;
SET @tables = NULL;
SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables
  FROM information_schema.tables
  WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@tables,'dummy') INTO @tables;

SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;";

Entity::staticQuery($query);