<?php

namespace Fabacks\Entity\Connector;
interface IConnector
{
    public static function type();
    public static function select();
    public static function insert();
	public static function update();
    public static function delete();
    public static function create();
	public static function count();
    public static function drop();
    public static function truncate();
    public static function addColumn();
    public static function dropColumn();

}