<?php

/**
 * @description Mysql est une base de donnée SQL.
 * @author fabacks
 * @license copyright
 */

namespace Fabacks\Entity\Connector;
class Mysql
{
	const label = 'MySQL';
	const connection = 'mysql:host={{host}}; dbname={{name}}; port={{port}}';

	//TODO : rajouter le type par défaut DEFAULT
	/**
	 * Type des colonnes
	 * @return array 
	 */
	public static function types()
	{
		$types = array();

		$types['key'] = array(
			'type'		=> 'key',
			'template' 	=> 'int(:size) NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'size'		=> 11,
			'nullable'	=> false,
			'isNull' 	=> '',
			'isNotNull' => ''
		);

		$types['string'] = $types['varchar'] = array(
			'type'		=> 'string',
			'template' 	=> 'VARCHAR(:size) :nullable',
			'size'     	=> 225,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);
		
		$types['longstring'] = $types['text'] = array(
			'type'		=> 'text',
			'template' 	=> 'TEXT :nullable',
			'size'     	=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['json'] = array(
			'type'		=> 'json',
			'template' 	=> 'JSON :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);
		
		$types['bool'] = $types['boolean'] = array(
			'type'		=> 'bool',
			'template' 	=> 'INT(1) :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['int'] = $types['integer'] = array(
			'type'		=> 'int',
			'template' 	=> 'INT(:size) :nullable',
			'size'		=> 11,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);
		
		$types['bigint'] = $types['long'] = array(
			'type'		=> 'bigint',
			'template' 	=> 'BIGINT :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['float'] = array(
			'type'		=> 'float',
			'template' 	=> 'FLOAT :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);			 

		$types['time'] = array(
			'type'		=> 'time',
			'template' 	=> 'TIME :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['timestamp'] = array(
			'type'		=> 'timestamp',
			'template' 	=> 'BIGINT :nullable', //'TIMESTAMP :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['default'] = array(
			'type'		=> 'default',
			'template' 	=> 'TEXT NOT NULL COLLATE',
			'size'		=> null,
			'nullable'	=> null,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		return $types;
	}


}