<?php
/**
 * @description Sqlite est une base de données SQL mono-fichier.
 * @author Fabacks
 * @license copyright
 */

namespace Fabacks\Entity\Connector;

class Sqlite 
{
	const label = 'Sqlite';
	const connection = 'sqlite:{{host}}{{name}}';

	public static function types()
	{
		$types = array();

		$types['key'] = array(
			'type'		=> 'key',
			'template' 	=> 'INTEGER NOT NULL PRIMARY KEY',
			'size' 		=> 255,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['string'] = array(
			'type'		=> 'string',
			'template' 	=> 'VARCHAR(:size) :nullable',
			'size' 		=> 255,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['longstring'] = $types['text'] = array(
			'type'		=> 'text',
			'template' 	=> 'TEXT :nullable',
			'size'     	=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['bool'] = $types['boolean'] = array(
			'type'		=> 'bool',
			'template' 	=> 'INT(1) :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['int'] = $types['integer'] = array(
			'type'		=> 'int',
			'template' 	=> 'INT(:size) :nullable',
			'size'		=> 11,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['bigint'] = $types['long'] = array(
			'type'		=> 'bigint',
			'template' 	=> 'BIGINT :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['float'] = array(
			'type'		=> 'float',
			'template' 	=> 'FLOAT :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['time'] = array(
			'type'		=> 'time',
			'template' 	=> 'VARCHAR(8) :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['timestamp'] = array(
			'type'		=> 'timestamp',
			'template' 	=> 'INTEGER :nullable',
			'size'		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['blob'] = array(
			'type'		=> 'blob',
			'template' 	=> 'BLOB',
			'size' 		=> null,
			'nullable'	=> null,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);

		$types['default'] = array(
			'type'		=> 'default',
			'template' 	=> 'TEXT',
			'size' 		=> null,
			'nullable'	=> false,
			'isNull' 	=> 'NULL',
			'isNotNull' => 'NOT NULL'
		);
		
		return $types;
	}


}