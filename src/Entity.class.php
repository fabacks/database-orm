<?php

namespace Fabacks\Entity;
use \Exception;

/**
 * Classe ORM parent de tous les modèles (classe entités) liées a la base de donnée,
 * Cette classe fonctionne avec les base de donnée Sqlite et mysql.
 * Projet original par Valentin CARRUESCO Aka idleman 
 * @author Fabacks
 * @category Core
 * @license cc by nc sa
 * @version 1.3.1
 * @date 07/10/2021
*/
class Entity
{
	private $pdo = null;
	private $connector = null;
	private $PDOError = '';
	protected $TABLE_NAME = null;
	public static $last = array('query' => '', 'error' => '');


	function __construct()
	{
		$this->pdo = EntityConnector::getInstance()->connection;
		if(!isset($this->TABLE_NAME)):
			$this->TABLE_NAME = strtolower(get_called_class());
		endif;

		$this->connector = EntityConnector::ENTITY_TYPE;
	}

	public function __toString()
	{
		foreach($this->toArray() as $key=>$value):
			echo $key.' : '.$value.','.PHP_EOL;
		endforeach;
	}

	public function __get($name)
	{
		$pos = strpos($name,'_object');

		if($pos!==false){
			$field = strtolower(substr($name,0,$pos));
			if(array_key_exists($field, $this->fields)){
				$class = ucfirst($field);
				return $class::getById($this->{$field});
			}
		}
    
		throw new Exception("Attribut ".get_class($this)."->$name non existant");
	}

	function toArray()
	{
		$fields = array();
		foreach($this->fields as $field=>$type)
			$fields[$field]= $this->$field;
		return $fields;
	}

	function fromArray($array)
	{
		foreach($array as $field=>$value)
			$this->$field = $value;
	}

	/**
	 * Retourne le paramétrage du type de la colonne 
	 * Exemple : VARCHAR(255) NULL
	 * 
	 * @param string|array $type 
	 * @return string 
	 */
	private function sgbdType($type)
	{
		$infoTypes = array();
    
		$pathConnect = __DIR__.DIRECTORY_SEPARATOR.'Connector'.DIRECTORY_SEPARATOR;
		require_once $pathConnect.'IConnector.interface.php';

		switch( $this->connector ):
			case EntityConnector::TYPE_MYSQL:
				require_once $pathConnect.'Mysql.class.php';
				$infoTypes = \Fabacks\Entity\Connector\Mysql::types();
			break;
			case EntityConnector::TYPE_SQLITE:
				require_once $pathConnect.'Sqlite.class.php';
				$infoTypes = \Fabacks\Entity\Connector\Sqlite::types();
			break;
			default:
				throw new \Exception("Le type de SGBD est incorrecte");
		endswitch;
    
		return self::sgbdTypeRender($infoTypes, $type);
	}

	/**
	 * Effectue le rendu du type de paramétrage 
	 * 
	 * @param mixed $infoTypes 
	 * @param mixed $pType 
	 * @return string 
	 */
	private static function sgbdTypeRender($infoTypes, $pType) 
	{
		$dataType = is_string($pType) ? $pType : $pType['type'];

		if( empty($infoTypes[$dataType]) )
			throw new \Exception("Impossible de determiner le type de donnée");
		
		$tpl = $infoTypes[$dataType]['template'];

		// On définie la taille du type
		if( $infoTypes[$dataType]['size'] !== null ):
			$replace = $pType['size'] ?? $infoTypes[$dataType]['size'];
			$tpl = str_replace(':size', $replace, $tpl);
		endif;

		// On définie si le type est nullable
		if( $infoTypes[$dataType]['nullable'] !== null ):
			$nullable = $pType['nullable'] ?? $infoTypes[$dataType]['nullable'];
			$replace  = $nullable ? $infoTypes[$dataType]['isNull'] : $infoTypes[$dataType]['isNotNull'];
			$tpl = str_replace(':nullable', $replace, $tpl);
		endif;

		return $tpl;
	}

	/**
	 * Ferme la connexion à la base de donnée 
	 * 
	 * @return void 
	 */
	public function closeDatabase()
	{
		$this->pdo = null;
	}


	/**
	 * Retourne le nom de la table avec son préfix
	 *
	 * @param boolean $withPrefix default true Retourne avec le prefix
	 * @return string
	 */
	public static function tableName($withPrefix = true)
	{
		$class = get_called_class();
		$instance = new $class();
		return ($withPrefix ? EntityConnector::ENTITY_PREFIX : "" ).$instance->TABLE_NAME;
	}

	/**
	 * Affichage l'erreur lors de la query
	 * 
	 * @return void 
	 */
	public function debug()
	{
		echo '<pre><code>';
		print_r($this::$PDOError);
		echo '</code></pre>';
	}

	/**
	 * Affichage la dernière erreur / query
	 * 
	 * @return void 
	 */
	public final static function debugLastError()
	{
		echo '<pre><code>';
		print_r(self::$last);
		echo '</code></pre>';
	}

	
	
	
	/* ------------------------------------
	------------- GESTION SQL -------------
	-------------------------------------*/

	/**
	 * Retourne la taille de la table en MB
	 *
	 * @return float
	 */
	public static function tableSize()
	{
		$tableName = self::tableName();
		$bdd = EntityConnector::ENTITY_NAME;
		$query = 
			"SELECT 
				table_schema as `Database`, 
				table_name AS `Table`, 
				(data_length + index_length) AS `size` 
			FROM information_schema.TABLES 
			WHERE table_schema = '$bdd' 
				AND TABLE_NAME = '$tableName';";

		$res = self::staticQuery($query)->fetch();
		return $res ? (double)$res["size"] : 0;
	} 


	/**
	* Vérifie l'existence de la table en base de donnée
	*
	* @param bool $autocreate créé la table si elle n'existe pas
	* @return bool true si la table existe, false dans le cas contraire
	*/
	public static function checkTable($autocreate = false)
	{
		$return = false;
		$class = get_called_class();
		$instance = new $class();
		$query = 'SELECT count(*) AS numRows FROM sqlite_master WHERE type="table" AND name="'.$instance->tableName().'"';  
		$statement = $instance->customQuery($query);

		if( $statement != false ):
			$statement = $statement->fetchArray();
			if( $statement['numRows'] == 1 ):
				$return = true;
			endif;
		endif;

		if($autocreate && !$return):
			self::create();
		endif;

		return $return;
	}

	/**
	 * Crée toutes les entités qui sont dans le dossier passé en paramètre
	 *
	 * @param string $pDirectory Path complet vers le dossier
	 * @return void
	 */
	public static function install($pDirectory)
	{
		$files = glob($pDirectory.DIRECTORY_SEPARATOR.'*.class.php');
		foreach($files as $file) :
			$infos = explode('.', basename($file));
			$class = array_shift($infos);

			if (!class_exists($class) || $class == get_class()) 
				continue;

			if( !method_exists($class, 'create') || !property_exists($class, 'fields') || !property_exists($class, 'TABLE_NAME') )
				continue;

			$class::create();
		endforeach;
		unset($files);
	}

	/**
	 * Supprime toutes les entités qui sont dans le dossier passé en paramètre
	 *
	 * @param string $pDirectory  Path complet vers le dossier
	 * @return void
	 */
	public static function uninstall($pDirectory)
	{
		$files = glob($pDirectory.DIRECTORY_SEPARATOR.'*.class.php');
		foreach($files as $file) :
			$infos = explode('.', basename($file));
			$class = array_shift($infos);
			
			if (!class_exists($class) || !method_exists($class, 'drop') || $class == get_class()) 
				continue;
			
			$class::drop();
		endforeach;
		unset($files);
	}

	/**
	* Méthode de création de l'entité en base
	*
	* @return bool
	*/
	public static function create()
	{
		$class = get_called_class();
		$instance = new $class();
		$query = 'CREATE TABLE IF NOT EXISTS `'.$instance->tableName().'` (';

		foreach($instance->fields as $field=>$typeItem):
			$type = $instance->sgbdType($typeItem);
			$query .='`'.$field.'`  '.$type.',';
		endforeach;

		$query = substr($query, 0, strlen($query)-1);
		$query .= ');';

		$result = $instance->customExecute($query);
		return $result == false ? false : true;
	}

	/**
	 * Suppression de l'entité en base
	 *
	 * @return bool
	 */
	public static function drop()
	{
		$class = get_called_class();
		$instance = new $class();
		$query = 'DROP TABLE IF EXISTS `'.$instance::tableName().'`;';
		
		$result = $instance->customExecute($query);
		return $result == false ? false : true;
	}

	/**
     * Supprime toute les données de la table
     *
     * @return bool
     */
    public static function truncate()
    {
        $class = get_called_class();
        $instance = new $class();
        $query = 'TRUNCATE TABLE `'.$instance->tableName().'`;';
		
		$result = $instance->customExecute($query);
		return $result;
    }

	/**
	 * Ajoute une colonne à la table
	 * 
	 * @param string $column Nom de la colonne à ajouter
	 * @param string|array $type Type à ajouter
	 * @param string $columnAfter Nom de la colonne pour l'ajouter après celle-ci
	 * @return bool 
	 */
	public static function addColumn($column, $type, $columnAfter = null)
	{
		$entity = new self();
		$type  = $entity->sgbdType($type);
		$sql   = "ALTER TABLE `".self::tableName()."` ADD COLUMN `$column` $type";
		if( $columnAfter != null):
			$sql .= " AFTER `$columnAfter`";
		endif;
		$sql .= ';';

		$resp = $entity->customQuery($sql);
		return $resp == false ? false : true;
	}

	/**
	 * Supprime une colonne dans la table
	 * 
	 * @param string $column 
	 * @return bool
	 */
	public static function dropColumn($column)
	{
		$sql = "ALTER TABLE `".self::tableName()."` DROP COLUMN `".$column."`;";

		$resp = self::staticQuery($sql);
		return $resp == false ? false : true;
	}
	
	/**
	 * Permet d'insérer massivement des entités en base
	 *
	 * @param array $objectArray Un tableau d'entité
	 * @param boolean $forceId
	 * @return bool
	 */
	public static function massiveInsert($objectArray, $forceId = false)
	{
		$class = get_called_class();
		$instance = new $class();
		$query = 'INSERT INTO `'.$instance->tableName().'`(';
		
		foreach($instance->fields as $field=>$type){
			if($type=='key' && !$forceId) continue;
			$query .='`'.$field.'`,';
		}

		$query = substr($query,0,strlen($query)-1);
		$query .=') VALUES (';
		$u = false;
		foreach($objectArray as $obj):
			if($u):  
				$query .='),('; 
			else : 
				$u=true; 
			endif;
		
			foreach($obj->fields as $field=>$type) :
				if($type == 'key' && !$forceId) continue;

				$query .= '"'.addslashes( self::encodeHtml($obj->{$field}) ).'",';
			endforeach;

			$query = substr($query, 0, strlen($query) -1);
		endforeach;

		$query .=');';
		
		$result = $instance->customExecute($query);
		return $result == false ? false : true;
	}

	/**
	* Méthode d'insertion ou de modifications d'éléments de l'entité
	*
	* @return int|bool L'ID de l'entity | false si une erreur
	*/
	public function save()
	{
		if(isset($this->id)){
			$query = 'UPDATE `'.$this->tableName().'` SET ';
			foreach($this->fields as $field => $typeItem):
				$type = is_array($typeItem) ? $typeItem['type'] : $typeItem;
				$isNullable = is_array($typeItem) && array_key_exists('nullable', $typeItem) ? $typeItem['nullable'] : false;
				
				if($type == 'key') 
					continue;
				
				if( is_null($this->{$field}) && $isNullable) {
					$query .= '`'.$field.'`= null';
				}else if($type == "bool") {
					$val = ($this->{$field} ? 1 : 0);
					$query .= '`'.$field.'`="'.$val.'"';
				}else if($type == "json") {
					$query .= '`'.$field.'`= \''.$this->{$field}.'\'';
				}else {
					$id = self::encodeHtml($this->{$field});
					$query .= '`'.$field.'`="'.$id.'"';
				}		
				
				$query .= ',';
			endforeach;
			
			$query = substr($query,0,strlen($query)-1);
			$query .= ' WHERE `id`="'.$this->id.'";';
		}else {
			$query = 'INSERT INTO `'.$this->tableName().'`(';
			foreach($this->fields as $field=>$type):
				if($type != 'key')
					$query .='`'.$field.'`,';
			endforeach;
			
			$query = substr($query,0,strlen($query)-1);
			$query .=')VALUES(';

			foreach($this->fields as $field => $typeItem):
				$type = is_array($typeItem) ? $typeItem['type'] : $typeItem;
				$isNullable = is_array($typeItem) && array_key_exists('nullable', $typeItem) ? $typeItem['nullable'] : false;

				if($type == 'key') 
					continue;

				if( is_null($this->{$field}) && $isNullable ) {
					$query .= 'null';
				}else if($type == "bool") {
					$val = ($this->{$field} ? 1 : 0);
					$query .= '"'.$val.'"';
				}else if($type == "json" || $type == "jsonnull" ) {
					$query .= '\''.$this->{$field}.'\'';
				}else {
					$query .= '"'.self::encodeHtml($this->{$field}).'"';
				}

				$query .= ',';					
			endforeach;

			$query = substr($query,0,strlen($query)-1);
			$query .=');';
		}

		$result = $this->customExecute($query);

		// En cas d'erreur
		if( $result === false ):
			$this->PDOError = self::$last;
			return false;
		endif;

		// Un INSERT
		if( is_numeric($result) && intval($result) > 0 ):
			$this->id = $result;
			return $this->id;
		endif;

		// En UDPATE
		if( is_numeric($result) && intval($result) == 0 ):
			return $this->id;
		endif;

		// Si cas non définie
		return false;
	}

	/**
	* Méthode de modification d'éléments de l'entité
	*
	* @param array $columns Tableau de changement des colonnes ($colonnes => $valeurs)
	* @param array $where  Tableau de condition ($colonnes => $valeurs) (WHERE)
	* @param string $operation définis le type d'operateur pour la requête select
	* @return bool
	*/
	public static function change(array $columns, array $where = null, $operation = '=')
	{
		$class = get_called_class();
		$instance = new $class();
		$query = 'UPDATE `'.$instance->tableName().'` SET ';

		foreach( $columns as $column => $value ):
			$query .= '`'.$column.'`="'.$value.'",';
		endforeach;
		$query = substr($query, 0, strlen($query)-1);
		
		if( $where != null ):
			$query .= ' WHERE '; 
			
			foreach( $where as $column => $value ):
				$query .= '`'.$column.'`'.$operation.'"'.$value.'" AND ';
			endforeach;

			$query = substr($query, 0, strlen($query)-5); // -5 => " AND " soit 5 caractères
		endif;

		$result = $instance->customExecute($query);
		return $result == false ? false : true;
	}

	/**
	* Méthode de selection multiple d'éléments de l'entité
	*
	* @param array $columns (WHERE) Tableau (colonne => valeur) de selection
	* @param string $order Trie des réponses (exemple : "login ASC")
	* @param string $limit Nombre de réponse
	* @param string $operation Définis le type d'operateur pour la requête select
	* @param string $selColumn Selection des colonnes
	* @return array[static]|false $Entity
	*/
	public static function loadAll($columns=array(), $order=null, $limit=null, $operation="=", $selColumn='*')
	{
		$whereClause = '';

		if($columns!=null && sizeof($columns)!=0){
			$whereClause .= ' WHERE ';
			$i = false;
			foreach($columns as $column=>$value){
				if($i){$whereClause .=' AND ';}else{$i=true;}
				$whereClause .= '`'.$column.'`'.$operation.'"'.$value.'"';
			}
		}

		$class = get_called_class();
		$instance = new $class();
		$query = 'SELECT '.$selColumn.' FROM `'.$instance->tableName().'` '.$whereClause.' ';
		
		if($order!=null) $query .='ORDER BY '.$order.' ';
		if($limit!=null) $query .='LIMIT '.$limit.' ';
		$query .=';';

		return $instance->customQuery($query, true);
	}


	/**
	* Méthode de sélection unique d'éléments de l'entité
	*
	* @param array $columns (WHERE) Tableau (colonne => valeur) de sélection
	* @param string $operation Définis le type d'operateur pour la requête select
	* @return static|false Entity ou false si aucun objet n'est trouvé en base
	*/
	public static function load($columns, $operation='=')
	{
		$objects = self::loadAll($columns, null, '1', $operation);
		return isset($objects[0]) ? $objects[0] : false;
	}

	/**
	* Méthode de sélection unique d'éléments de l'entité via son ID
	*
	* @param int $id de l'entité
	* @param string $operation Définis le type d'operateur pour la requête select
	* @return static|false $Entity ou false si aucun objet n'est trouvé en base
	*/
	public static function getById($id, $operation = '=')
	{
		return self::load(array('id'=>$id), $operation);
	}

	/**
	 * Méthode de comptage des éléments de l'entité
	 * 
	 * @param array $columns (WHERE) Tableau (colonne => valeur) de sélection
	 * @return int Nombre de ligne dans l'entité
	 */
	public static function rowCount($columns = null)
	{
		$class = get_called_class();
		$instance = new $class();
		$whereClause ='';
		if($columns!=null){
			$whereClause = ' WHERE ';
			$i=false;
			foreach($columns as $column=>$value){
				if($i){$whereClause .=' AND ';}else{$i=true;}
				$whereClause .= '`'.$column.'`="'.$value.'"';
			}
		}

		$query = 'SELECT COUNT(id) resultNumber FROM `'.$instance->tableName().'`'.$whereClause;
		$execQuery = $instance->customQuery($query);
		if( $execQuery == false )
			return 0;

		$row = $execQuery->fetch();
		return $row['resultNumber'];
	}

	/**
	* Méthode de définition de l'existence d'un moins un des éléments spécifiés en base
	*
	* @return bool existe (true) ou non (false)
	*/
	public static function exist($columns=null) 
	{
		$result = self::rowCount($columns);

		return ($result != 0);
	}	

	/**
	 * Supprime un élément via son id
	 *
	 * @param integer $id
	 * @return bool
	 */
	public static function deleteById($id)
	{
		return self::delete(array('id'=>$id));
	}

	/**
	 * Méthode de suppression d'éléments de l'entité
	 *
	 * @param array $columns (WHERE) Tableau (colonne => valeur) de sélection
	 * @param string $operation 
	 * @param int|null $limit 
	 * @return int|bool 
	 */
	public static function delete($columns, $operation='=', $limit=null)
	{
		$class = get_called_class();
		$instance = new $class();
		$whereClause = '';
		$i = false;
		foreach($columns as $column=>$value):
			if($i){
				$whereClause .=' AND ';
			} else{
				$i=true;
			}
			$whereClause .= '`'.$column.'`'.$operation.'"'.$value.'"';
		endforeach;
		$query = 'DELETE FROM `'.$instance->tableName().'` WHERE '.$whereClause.' '.(isset($limit)?'LIMIT '.$limit:'').';';
		
		$resp = $instance->customExecute($query);
		return $resp === false ? false : true;
	}

	/**
	 * Exécute une requête SQL interne sans retour d'information
	 *
	 * @param string $query
	 * @return int|bool
	 */
	protected function customExecute($query)
	{
		self::$last['query'] = $query;
		self::$last['error'] = '';

		try {
			$this->pdo->exec($query);

			// When no exception is thrown, lastInsertId returns 0. However, if lastInsertId is called before calling commit, the right id is returned.
			$lastId = $this->pdo->lastInsertId();
			return $lastId;
		} catch (\Exception $e) {
			self::$last['error'] = $e->getMessage();

			$this->closeDatabase();
            return false;
		} 
	}

	/**
	 * Exécute une requête SQL préparé interne sans retour d'information
	 *
	 * @param string $query
	 * @param array $query array[key] = value
	 * @return int|bool
	 */
	protected function customPrepareExecute($query, array $datas)
	{
		self::$last['query'] = $query;
		self::$last['error'] = '';
		$this->pdo->beginTransaction();

		try {
			$stmt = $this->pdo->prepare($query);
			foreach($datas as $key => $value):
				$stmt->bindValue(':'.$key, $value); 
			endforeach;

			$stmt->execute($query);
			$this->pdo->commit();
			
			// When no exception is thrown, lastInsertId returns 0. However, if lastInsertId is called before calling commit, the right id is returned.
			$lastId = $this->pdo->lastInsertId();
			return $lastId;
		} catch (\Exception $e) {
			self::$last['error'] = $e->getMessage();

			$this->pdo->rollBack();
            return false;
		} 
	}


	/**
	 * 	Permet d'exécuter une requête personnalisé avec un objet statique 
	 *
	 * @param string $query
	 * @param boolean $fill Mappage avec l'entité qui execute la requête
	 * @param boolean $isFetchAll => fetchAll sur le jeu de résultat
	 * @return array|object
	 */
	public static function staticQuery($query, $fill = false, $isFetchAll = false)
	{
		$class = get_called_class();
		$instance = new $class();
		return $instance->customQuery($query, $fill, $isFetchAll);
	}

	/**
	 * 	Permet d'exécuter une requête personnalisé et mappe les données sur l'entité
	 *
	 * @param string $query
	 * @return array|false
	 */
	public static function staticQueryFill($query)
	{
		$class = get_called_class();
		$instance = new $class();
		return $instance->customQuery($query, true);
	}

	/**
	 * Permet d'exécuter une requête personnalisé et fait un fetchAll
	 * Ne peut être faite sur une entité directement
	 * 
	 * @param string $query
	 * @return array|false
	 */
	public final static function staticQueryFetchAll($query)
	{
		$class = get_called_class();
		$instance = new $class();
		$results = $instance->customQuery($query);
		return !$results ? false : $results->fetchAll();
	}

	/**
	 * Permet d'exécuter une requête personnalisé et fait un fetch
	 * Retourne la première ligne du tableau de résultat
	 * 
	 * @param string $query
	 * @return array|false
	 */
	public final static function staticQueryFetch($query)
	{
		$class = get_called_class();
		$instance = new $class();
		$results = $instance->customQuery($query);
		return !$results ? false : $results->fetch();
	}

	/**
	 * Permet d'exécuter une requête avec retour de données
	 *
	 * @param string $query
	 * @param boolean $fill
	 * @param boolean $idKey
	 * @param boolean $isFetchAll => fetchAll sur le jeu de résultat TODO : a supprimer à terme pour utiliser staticQueryFetchAll
	 * @return object|array|false
	 */
	protected function customQuery($query, $fill = false, $isFetchAll = false) 
	{
		self::$last['query'] = $query;
		self::$last['error'] = '';

		try {
			$results = $this->pdo->query($query);

			if( $fill == false ) :
				return $isFetchAll ? $results->fetchAll() : $results;
			endif;

			$class = get_class($this);
			$objects = array();
			while($queryReturn = $results->fetch())
			{
				$object = new $class();

				foreach($this->fields as $field=>$type):
					if(!isset($queryReturn[$field]))
						continue;

					$object->{$field} =  $queryReturn[$field];
				endforeach;

				$objects[] = $object;
				unset($object);
			}

			return empty($objects) ? false : $objects;
		} catch (\PDOException $e) {
			self::$last['error'] = $e->getMessage();

            return false;
		}
	}


	/**
	 * Encode la valeur pour la base de donnée 
	 *
	 * @param string $value
	 * @return string
	 */
	public static function encodeHtml($value)
	{
		if( $value === null ) return '';

		//return htmlentities($value, ENT_QUOTES, "UTF-8", false);
		return htmlspecialchars($value, ENT_QUOTES, "UTF-8", false);
	}

	/**
	 * Encode en json avec les bon paramètre pour être lus en SLQ
	 *
	 * @return json
	 */
	public static function json_encode($data)
	{
		// $data = str_replace("\n", "\\n", $data);
		$data = self::dooblen_data($data);
		return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}

	/**
	 * Encode les datas pour la fonction Json
	 * @param mixed $data 
	 * @return array|object|string 
	 */
	private static function dooblen_data($data)
	{
		if(is_array($data)) :
			foreach($data as $key => $item):
				$data[$key] = self::dooblen_data($item);
			endforeach;
		elseif(is_object($data)):
			foreach ($data as $key => $value) :
				$data->$key = self::dooblen_data($value);
			endforeach;
		else :
			$data = str_replace("\n", "\\n", $data);
			$data = self::encodeHtml($data);
		endif;

		return $data;
	}

}