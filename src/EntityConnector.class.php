<?php

namespace Fabacks\Entity;
use \Exception;
use \PDO;

/**
* @description PDO Connector for database connexion.
* @author Fabien COLAS Aka Fabacks
* @site dahoo.Fr
* @git https://github.com/Fabacks
* @category Core
* @license cc by nc sa
* @date 23/07/2019
*/
class EntityConnector
{
	const ENTITY_TYPE    = ENTITY_TYPE;				// Définie le type de SGBD (mysql, sqlite)
	const ENTITY_PREFIX  = ENTITY_PREFIX;			// Permet de préfixer toutes les tables
	const ENTITY_HOST    = ENTITY_HOST;				// HOST database | Chemin si sqlite
	const ENTITY_PORT    = ENTITY_PORT;				// Port par défaut Mysql 3306 | inutile si sqlite
	const ENTITY_LOGIN   = ENTITY_LOGIN;			// Vide si sqlite
	const ENTITY_PWD	 = ENTITY_MDP;           	// Vide si sqlite
	const ENTITY_NAME	 = ENTITY_BDD;           	// Le nom de la base de donnée : mysql / sqlite
	const ENTITY_CHARSET = 'utf8mb4'; 				// Charset (utf8, utf8mb4, ...)

	const TYPE_MYSQL 	 = "mysql";
	const TYPE_SQLITE 	 = "sqlite";

	public $connection = null;
	public static $instance = null;

	private function __construct() {
		$this->connect();
	}

	/**
	 * Méthode de récupération unique de l'instance PDO via EntityConnector
	 * 
	 * @category Singleton
	 * @return self $instance
	*/
	public static function getInstance()
	{
		if( self::$instance === null ):
			self::$instance = new self(); 
		endif;

		return self::$instance;
	}

	/**
	 * Sélection type de base et connexion
	 *
	 * @return void
	 */
	public function connect()
	{
		$pathConnect = __DIR__.DIRECTORY_SEPARATOR.'Connector'.DIRECTORY_SEPARATOR;
		require_once $pathConnect.'IConnector.interface.php';

		$connector = ucfirst(self::ENTITY_TYPE);
		require_once $pathConnect.$connector.'.class.php';

		$obj = '\Fabacks\Entity\Connector\\'.$connector;
		$connectDSN = $obj::connection;
		$entityValue = array(
			'host' 	=> self::ENTITY_HOST,
			'name' 	=> self::ENTITY_NAME,
			'port' 	=> self::ENTITY_PORT,
		);

		try {
			foreach($entityValue as $key => $value):
				$connectDSN = str_replace('{{'.$key.'}}', $value, $connectDSN);
			endforeach;

			$this->connection = new PDO($connectDSN, self::ENTITY_LOGIN, self::ENTITY_PWD);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			if( !in_array(self::ENTITY_TYPE, array(self::TYPE_SQLITE)) ):
				$this->connection->query('SET NAMES '.self::ENTITY_CHARSET);
			endif;

		} catch( Exception $e ) {
			echo "Connection à la base de donnée impossible : ", $e->getMessage();
			die();
		}
	}

	/**
	 * Permet de modifier l'instance PDO pour raoujer par exemple un traceur
	 * 
	 * @param mixed $pPdo 
	 * @return void 
	 */
	public static function setPDO($pPdo)
	{
		self::$instance->connection = $pPdo;
	}

	/**
	 * Test si on est bien en type SqlLite est que la base existe
	 *
	 * @return boolean
	 */
	public static function isSqlLiteExist()
	{
		if( strtolower(self::ENTITY_TYPE) != 'sqlite')
			return false;

		$path = self::getPathSQLite();
		return file_exists($path);
	}

	/**
	 * Retourne le chemin si sqlite
	  * 
	  * @param bool $folderOnly Retourne uniquement le dossier d'accès
	  * @return false|string 
	  */
	public static function getPathSQLite($folderOnly = false)
	{
		if( strtolower(self::ENTITY_TYPE) != 'sqlite')
			return false;

		return self::ENTITY_HOST.($folderOnly ? '' : self::ENTITY_NAME);
	}
}